package com.example.legacy;



@javax.annotation.Generated(
	value = "com.tmaxsoft.sts4.codegen.dto.DtoGenerator",
	date= "22. 6. 27. 오후 4:23",
	comments = "UserDto"
)
public class UserDto
{
    
    private String empno = null;
    
    public String getEmpno() {
    	return empno;
    }	
    
    
    public void setEmpno(String empno) {
    	if(empno == null) {
    		this.empno = null;
    	} else {
    		this.empno = empno;
    	}
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	
    	buffer.append("empno : ").append(empno).append("\n");   
    	return buffer.toString();
    }
}

