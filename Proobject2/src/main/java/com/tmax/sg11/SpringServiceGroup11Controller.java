package com.tmax.sg11;

import com.tmax.sg11.so11.ServiceName11;
import com.example.legacy.UserDto;
import com.example.legacy.UserDtoMsgJson;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.common.base.Throwables;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.HttpStatus;
import jeus.proobject.model.network.context.RequestContext;
import jeus.proobject.core2.service.ServiceName;
import com.tmax.proobject.service.util.SpringControllerUtil;
import com.tmax.proobject.service.util.ProObjectHttpProtocol;
import com.tmax.proobject.service.context.ServiceContextHolder;

@javax.annotation.Generated(
	value = "com.tmaxsoft.sts4.codegen.controller.SpringControllerGenerator",
	date= "22. 6. 27. 오후 3:38",
	comments = ""
)
@Controller
public class SpringServiceGroup11Controller
{
    
    @Autowired
    ServiceName11 _ServiceName11;
    @RequestMapping(value="/serviceName11", method=RequestMethod.POST)
    public void executeServiceName11_methodName111 (HttpServletRequest request, HttpServletResponse response) throws Throwable {
        response.setContentType("application/json");
        
        UserDtoMsgJson inputMsgJson = new UserDtoMsgJson();
        ProObjectHttpProtocol protocol;
        try {
        	protocol = SpringControllerUtil.unmarshalRequestBody(request.getInputStream(), inputMsgJson);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Unmarshalling Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        }
            
        ServiceName serviceName = new ServiceName("ServiceGroup11App.ServiceGroup11.ServiceName11");
        RequestContext requestContext = SpringControllerUtil.createRequestContext(serviceName, protocol.getHeader());
        ServiceContextHolder.getServiceContext().setRequestContext(requestContext);
            
        UserDto svcInput = (UserDto)protocol.getDto();
        UserDto svcOutput = null;
            
        try {
            SpringControllerUtil.handlePreService(serviceName, requestContext, svcInput);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"PreProcessing Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            ServiceContextHolder.removeServiceContext();
            return;
        }
        
        try {
            svcOutput = _ServiceName11.methodName111(svcInput);
        } catch (Throwable e) {
            try {
                SpringControllerUtil.handleServiceError(serviceName, requestContext, svcInput, e); // rollback
            } catch (Throwable e1) {
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
                String errorMsg = "{\"exception\" : \"ServiceError Handling Failed : " + Throwables.getStackTraceAsString(e1) + "\"}";
                response.getOutputStream().write(errorMsg.getBytes());
                ServiceContextHolder.removeServiceContext();
                return;
            }
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Service Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            ServiceContextHolder.removeServiceContext();
            return;
        }
        
        try {
            SpringControllerUtil.handlePostService(serviceName, requestContext, svcInput, svcOutput); // commit
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Post-Processing Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        } finally {
            ServiceContextHolder.removeServiceContext();
        }
        
        protocol.setDto(svcOutput);
        UserDtoMsgJson outputMsgJson = new UserDtoMsgJson();
        
        try {
            byte[] outputProtocolBytes = SpringControllerUtil.marshalResponseBody(protocol, outputMsgJson);
            response.getOutputStream().write(outputProtocolBytes);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Marshalling Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        }
        
        return;
    }
}

