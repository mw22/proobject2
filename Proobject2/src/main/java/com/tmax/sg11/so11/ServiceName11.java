package com.tmax.sg11.so11;

import org.springframework.stereotype.Service;
import com.example.legacy.UserDto;

@javax.annotation.Generated(
	value = "com.tmaxsoft.sts4.codegen.service.ServiceGenerator",
	date= "22. 6. 27. 오후 3:38",
	comments = ""
)
@Service
public class ServiceName11{
    
        public UserDto methodName111(UserDto input) throws Throwable {
        	System.out.println("methodName111 call ");
        	System.out.println("methodName111 input : " + input.getEmpno());
        	UserDto userDto = new UserDto();
        	userDto.setEmpno("test empno");
        	return userDto; 
        }
        
}

