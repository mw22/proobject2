package com.tmax.sg11;

import com.tmax.sg11.so11.ServiceName11;
import com.example.legacy.UserDto;
import com.example.legacy.UserDtoMsgJson;
import org.springframework.stereotype.Component;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import jeus.proobject.model.network.context.RequestContext;
import jeus.proobject.core2.service.ServiceName;
import jeus.proobject.model.context.Header;
import com.tmax.proobject.runtime.ProObjectControllerInput;
import com.tmax.proobject.service.util.ProObjectControllerUtil;
import com.tmax.proobject.service.context.ServiceContextHolder;
import com.tmax.proobject.runtime.msg.HeaderMsgJson;

@javax.annotation.Generated(
	value = "com.tmaxsoft.sts4.codegen.controller.POControllerGenerator",
	date= "22. 6. 27. 오후 3:38",
	comments = ""
)
@Component(value="ProObjectServiceGroup11Controller")
@EnableAutoConfiguration
public class ProObjectServiceGroup11Controller{
    
    @Autowired
    ServiceName11 _ServiceName11;
    public Object executeServiceName11(ProObjectControllerInput controllerInput) throws Throwable {
    	ServiceName serviceName = controllerInput.getServiceName();
    	RequestContext requestContext = controllerInput.getRequestContext();
    
    	ServiceContextHolder.getServiceContext().setRequestContext(requestContext);
    
    	boolean fromDispatcher = (controllerInput.getServiceInputObject() == null);
    
    	UserDto svcInput;
    	if (fromDispatcher) {
    		HeaderMsgJson headerMsg = new HeaderMsgJson();
    		Header header = headerMsg.unmarshal(controllerInput.getHeaderBytes());
    		requestContext.getRequest().setHeader(header);
    		UserDtoMsgJson svcInputMsgJson= new UserDtoMsgJson();
    		svcInput = svcInputMsgJson.unmarshal(controllerInput.getServiceInputBytes());
    	} else {
    		svcInput = (UserDto) controllerInput.getServiceInputObject();
    	}
    	ProObjectControllerUtil.handlePreService(serviceName, requestContext, svcInput); // getTransaction
    	UserDto svcOutput = null;
    	try {
    		svcOutput = _ServiceName11.methodName111(svcInput);
    		ProObjectControllerUtil.handlePostService(serviceName, requestContext, svcInput, svcOutput, fromDispatcher); // commit
    	} catch (Throwable e) {
    		ProObjectControllerUtil.handleServiceError(serviceName, requestContext, svcInput, e, fromDispatcher); // rollback
    		throw e;
    	}
    	ServiceContextHolder.removeServiceContext();
    	if (fromDispatcher) {
    		UserDtoMsgJson svcOutputMsgJson= new UserDtoMsgJson();
    		return svcOutputMsgJson.marshal(svcOutput);
    	} else {
    	return svcOutput;
    	}
    }
}

